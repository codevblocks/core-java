package com.codevblocks.core.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class StringUtil {

    public static final String REGEX_ALPHA_NUMERIC = "[a-zA-Z0-9]+";
    public static final String REGEX_NUMERIC = "[0-9]+";
    public static final String REGEX_EMAIL = "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b";
    public static final String REGEX_PHONE = "^(\\+)?([0-9]{1})([- 0-9]){5,19}$";
    public static final String REGEX_POSITIVE_DECIMAL = "^\\d+\\.?\\d*$";
    public static final String REGEX_DECIMAL = "[-+]?[0-9]*\\.?[0-9]+";

    private StringUtil() {}

    public static final String valueOf(final Object value) {
        return value != null ? String.valueOf(value) : null;
    }

    public static final int length(final String s) {
        return s != null ? s.length() : 0;
    }

    public static final String toCSV(final Object[] values) {
        return toSeparatedValues(values, 0, values != null ? values.length : 0, ",");
    }

    public static final String toCSV(final Object[] values, final int startIndex, final int endIndex) {
        return toSeparatedValues(values, startIndex, endIndex, ",");
    }

    public static final String toSeparatedValues(final Object[] values, final int startIndex, final int endIndex, final String separator) {
        if (values == null) {
            return null;
        }

        final StringBuilder csvBuilder = new StringBuilder();
        for (int i = startIndex ; i < endIndex ; ++i) {
            if (i > startIndex) {
                csvBuilder.append(separator);
            }
            csvBuilder.append(values[i]);
        }

        return csvBuilder.toString();
    }

    public static final String quotedString(final String value) {
        return String.format("'%1$s'", value);
    }

    public static final String keepLetters(final String s) {
        if (s == null) {
            return null;
        }

        final StringBuilder sBuilder = new StringBuilder();

        final int length = length(s);

        char c;
        for (int i = 0 ; i < length ; ++i) {
            c = s.charAt(i);
            if (Character.isLetter(c)) {
                sBuilder.append(c);
            }
        }

        return sBuilder.toString();
    }

    public static final String keepDigits(final String s) {
        if (s == null) {
            return null;
        }

        final StringBuilder sBuilder = new StringBuilder();

        final int length = length(s);

        char c;
        for (int i = 0 ; i < length ; ++i) {
            c = s.charAt(i);
            if (Character.isDigit(c)) {
                sBuilder.append(c);
            }
        }

        return sBuilder.toString();
    }

    public static final String md5(final String data) {
        return md5(data.getBytes());
    }

    public static final String md5(final byte[] data) {
        try {
            // Create MD5 Hash
            final MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(data);
            final byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (int i = 0 ; i < messageDigest.length ; i++) {
                hexString.append(String.format("%02x", 0xFF & messageDigest[i]));
            }

            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            // Should not happen
            throw new RuntimeException(e);
        }
    }

    public static final int safeParseInt(final String s, final int defaultValue) {
        if (length(s) > 0) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e) {
                /* DO NOTHING */
            }
        }

        return defaultValue;
    }

    public static final long safeParseLong(final String s, final long defaultValue) {
        if (length(s) > 0) {
            try {
                return Long.parseLong(s);
            } catch (NumberFormatException e) {
                /* DO NOTHING */
            }
        }

        return defaultValue;
    }

    public static final String safeValueOf(final Integer value) {
        return value != null ? String.valueOf((int) value) : null;
    }

    public static final String safeValueOf(final Long value) {
        return value != null ? String.valueOf((long) value) : null;
    }

    public static final String safeToUpperCase(final String s) {
        return s != null ? s.toUpperCase() : null;
    }

}