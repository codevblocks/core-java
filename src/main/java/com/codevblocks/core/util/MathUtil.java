package com.codevblocks.core.util;

public final class MathUtil {

    private MathUtil() {}

    public static final long greatestCommonDivisor(final long x, final long y) {
        return y == 0 ? x : greatestCommonDivisor(y, x % y);
    }

    public static final double distance(final double p1x, final double p1y, final double p2x, final double p2y) {
        return Math.sqrt(Math.pow(p2x - p1x, 2) + Math.pow(p2y - p1y, 2));
    }

}
