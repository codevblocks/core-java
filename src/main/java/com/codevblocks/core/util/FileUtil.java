package com.codevblocks.core.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

public final class FileUtil {

    private FileUtil() {}

    public static final Collection<File> listFilesInDirectory(final Collection<File> filesContainer, final File directory, final boolean includeDirectories, final boolean recursive) {
        final Collection<File> files = filesContainer != null ? filesContainer : new ArrayList<File>();

        if (directory.exists() && directory.isDirectory()) {
            final File[] directoryFiles = directory.listFiles();

            if (directoryFiles != null && directoryFiles.length > 0) {
                for (File directoryFile : directoryFiles) {
                    if (directoryFile.isDirectory()) {
                        if (includeDirectories) {
                            files.add(directoryFile);
                        }

                        if (recursive) {
                            listFilesInDirectory(files, directoryFile, includeDirectories, true);
                        }
                    } else {
                        files.add(directoryFile);
                    }
                }
            }
        }

        return files;
    }

    public static final void copy(final File source, final File destination) throws IOException {
        copy(source, destination, true);
    }

    public static final void copy(final File source, final File destination, final boolean overwrite) throws IOException {
        if (!source.exists()) {
            return;
        }

        if (destination.exists()) {
            if (!overwrite) {
                return;
            }

            destination.delete();
        }

        if (!destination.getParentFile().exists()) {
            destination.getParentFile().mkdirs();
        }

        InputStream in = null;
        OutputStream out = null;

        try {
            in = new BufferedInputStream(new FileInputStream(source));
            out = new BufferedOutputStream(new FileOutputStream(destination));

            final byte[] buffer = new byte[1024];

            int readLength;

            while ((readLength = in.read(buffer)) != -1) {
                out.write(buffer, 0, readLength);
            }
        } finally {
            if (in != null) { try { in.close(); } catch (Throwable t) { /* DO NOTHING */ } }
            if (out != null) { try { out.close(); } catch (Throwable t) { /* DO NOTHING */ } }
        }
    }

    public static final boolean delete(final File file) {
        boolean deleteResult = false;

        if (file.exists()) {
            deleteResult = true;

            if (file.isDirectory()) {
                final File[] directoryFiles = file.listFiles();
                final int directoryFilesCount = directoryFiles != null ? directoryFiles.length : 0;

                File directoryFile;
                for (int i = 0; i < directoryFilesCount; ++i) {
                    directoryFile = directoryFiles[i];

                    if (directoryFile.isDirectory()) {
                        deleteResult &= delete(directoryFile);
                    } else {
                        deleteResult &= directoryFile.delete();
                    }
                }

                deleteResult &= file.delete();
            } else {
                deleteResult = file.delete();
            }
        }

        return deleteResult;
    }

}
