package com.codevblocks.core.util;

import java.util.Comparator;
import java.util.Date;

public class SafeComparator<T extends Comparable<T>> implements Comparator<T> {

    public static final SafeComparator<String> STRING = new SafeComparator<String>();
    public static final SafeComparator<Integer> INTEGER = new SafeComparator<Integer>();
    public static final SafeComparator<Long> LONG = new SafeComparator<Long>();
    public static final SafeComparator<Float> FLOAT = new SafeComparator<Float>();
    public static final SafeComparator<Double> DOUBLE = new SafeComparator<Double>();
    public static final SafeComparator<Date> DATE = new SafeComparator<Date>();

    @Override
    public int compare(T t1, T t2) {
        if (t1 == null) {
            return ( t2 == null ? 0 : -1 );
        }
        if (t2 == null) {
            return 1;
        }
        return t1.compareTo(t2);
    }

}