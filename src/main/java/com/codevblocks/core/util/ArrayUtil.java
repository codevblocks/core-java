package com.codevblocks.core.util;

import java.util.ArrayList;
import java.util.List;

public final class ArrayUtil {

    private ArrayUtil() {}

    public static final <T> int length(final T[] array) {
        return array == null ? 0 : array.length;
    }

    public static final int indexOf(final byte[] array, final byte value) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == value) {
                    return i;
                }
            }
        }

        return -1;
    }

    public static final int indexOf(final short[] array, final short value) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == value) {
                    return i;
                }
            }
        }

        return -1;
    }

    public static final int indexOf(final int[] array, final int value) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == value) {
                    return i;
                }
            }
        }

        return -1;
    }

    public static final int indexOf(final long[] array, final long value) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == value) {
                    return i;
                }
            }
        }

        return -1;
    }

    public static final <T> int indexOf(final T[] array, final T value) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i].equals(value)) {
                    return i;
                }
            }
        }

        return -1;
    }

    public static final <T> List<T> asList(final T[] array) {
        if (array == null) {
            return null;
        }

        final int arrayLength = array.length;

        final List<T> list = new ArrayList<>(arrayLength);

        for (int i = 0 ; i < arrayLength ; ++i) {
            list.add(i, array[i]);
        }

        return list;
    }

    public static final <T> void fill(final List<T> list, final T value, final int count) {
        for (int i = 0 ; i < count ; ++i) {
            list.add(value);
        }
    }

}
