package com.codevblocks.core.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class ByteBufferOutputStream extends OutputStream {

    private ByteBuffer mBuffer;

    public ByteBufferOutputStream() {
        this.mBuffer = null;
    }

    public ByteBufferOutputStream(final ByteBuffer buffer) {
        this.mBuffer = buffer;
    }

    public final void setBuffer(final ByteBuffer buffer) {
        this.mBuffer = buffer;
    }

    public final ByteBuffer getBuffer() {
        return mBuffer;
    }

    @Override
    public final void write(final int b) throws IOException {
        mBuffer.put((byte) b);
    }

    @Override
    public final void write(final byte[] b) throws IOException {
        mBuffer.put(b, 0, b.length);
    }

    @Override
    public final void write(final byte[] b, final int off, final int len) throws IOException {
        mBuffer.put(b, off, len);
    }

}
